
const { production, development } = require('./knexfile');

const knex = require('knex')(
    process.env.NODE_ENV === 'production' ? production : development
)

async function placeOrder(order) {
    return new Promise((resolve, reject) => {
        return knex.transaction(async trx => {
            try {
                const isOrdersTableExists = await trx.schema.hasTable('orders');

                if (!isOrdersTableExists) {
                    await trx.schema.createTable('orders', table => {
                        table.increments('id');
                        table.integer('user_id');
                        table.decimal('total_amount');
                        table.timestamp('created_at').defaultTo(trx.fn.now());
                    });
                }

                const isProductInventoryTableExists = await trx.schema.hasTable('product_inventory');

                if (!isProductInventoryTableExists) {
                    await trx.schema.createTable('product_inventory', table => {
                        table.increments('id');
                        table.integer('product_id');
                        table.integer('quantity');
                    })

                }
                // Вставка замовлення в таблицю orders
                const [orderId] = await trx('orders').insert({
                    user_id: order.userId,
                    total_amount: order.totalAmount,
                    created_at: new Date()
                });

                // Вставка замовлення в таблицю product_inventory
                for (const item of order.items) {
                    await trx('product_inventory').insert({
                        product_id: item.productId,
                        quantity: item.quantity
                    });
                }

                await trx.commit(); // Підтвердження транзакції
                resolve(orderId);
            } catch (err) {
                await trx.rollback(); // Відкат транзакції у разі помилки
                reject(err);
            }
        });
    })
}

// Приклад використання
const newOrder = {
    userId: 1,
    totalAmount: 99.99,
    items: [
        { productId: 1, quantity: 2 },
        { productId: 3, quantity: 1 }
    ]
};

placeOrder(newOrder)
    .then(orderId => {
        console.log(`Замовлення ${orderId} успішно оформлене`);
    })
    .catch(err => {
        console.error('Помилка під час оформлення замовлення:', err);
    });