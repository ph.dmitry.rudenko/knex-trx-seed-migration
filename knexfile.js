// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

    development: {
      client: 'mysql',
      connection: {
          host: 'localhost',
          user: 'root',
          password: '00000000',
          database: 'trx'
      }
    },

    production: {
        client: 'mysql',
        connection: {
            host: 'localhost',
            user: 'root',
            password: '00000000',
            database: 'trx'
        }
      },
  };
  