/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    // add updated_at column to orders table
    return knex.schema.alterTable('orders', table => {
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    // remove updated_at column from orders table
    return knex.schema.alterTable('orders', table => {
        table.dropColumn('updated_at');
    })
};
