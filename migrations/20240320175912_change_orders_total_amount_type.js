/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    // change total_amount column type from decimal to integer

    return knex.schema.alterTable('orders', table => {
        table.integer('total_amount').alter()
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    // change total_amount column type from integer to decimal
    return knex.schema.alterTable('orders', table => {
        table.decimal('total_amount').alter()
    })
};
