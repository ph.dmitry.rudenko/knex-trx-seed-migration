/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('orders').del()
  await knex('orders').insert([
    {user_id: 1, total_amount: 99.99, created_at: new Date()},
    {user_id: 2, total_amount: 199.99, created_at: new Date()},
    {user_id: 3, total_amount: 299.99, created_at: new Date()},
    {user_id: 4, total_amount: 399.99, created_at: new Date()},
    {user_id: 5, total_amount: 499.99, created_at: new Date()},
  ]);
};
