/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('product_inventory').del()
  await knex('product_inventory').insert([
    {product_id: 1, quantity: 10},
    {product_id: 2, quantity: 20},
    {product_id: 3, quantity: 30},
  ]);
};
